find_program( ISPC_PROGRAM ispc )
if( "${ISPC_PROGRAM}" MATCHES "ISPC_PROGRAM-NOTFOUND" )
  message(FATAL_ERROR "ISPC Not Found")
else( )
  message(STATUS "ISPC Found: ${ISPC_PROGRAM}")
endif( )
set( ISPC_FLAGS ""
  CACHE STRING "ISPC compile flags" )
set( ISPC_FLAGS_DEBUG "-g -O0"
  CACHE STRING "ISPC debug compile flags" )
set( ISPC_FLAGS_RELEASE "-O2 -DNDEBUG"
  CACHE STRING "ISPC release compile flags" )
set( ISPC_FOUND 1 )

function( ispc_generate_object filename flags )

  set(output_object ${CMAKE_CURRENT_BINARY_DIR}/ispc/${filename}.o )
  set(output_header ${CMAKE_CURRENT_BINARY_DIR}/ispc/${filename}.h )

  if("${CMAKE_BUILD_TYPE}" MATCHES "Debug")
    set(ispc_compile_flags "${ISPC_FLAGS} ${ISPC_FLAGS_DEBUG} ${flags}")
  else("${CMAKE_BUILD_TYPE}" MATCHES "Release")
    set(ispc_compile_flags "${ISPC_FLAGS} ${ISPC_FLAGS_RELEASE} ${flags}")
  endif("${CMAKE_BUILD_TYPE}" MATCHES "Debug")
  string(REPLACE " " ";" ispc_compile_flags_list ${ispc_compile_flags})

  file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/ispc/)
  include_directories( ${CMAKE_CURRENT_BINARY_DIR} )


 add_custom_command(
    OUTPUT ${output_object}
    COMMAND ${ISPC_PROGRAM}
    -Iinclude ${ispc_compile_flags_list}
    ${CMAKE_CURRENT_SOURCE_DIR}/${filename}
    -o ${output_object} -h ${output_header}
    DEPENDS ${filename})

  set_source_files_properties(${output_object} PROPERTIES GENERATED TRUE)
  set_source_files_properties(${output_header} PROPERTIES GENERATED TRUE)

endfunction( )

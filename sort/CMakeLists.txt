ispc_generate_object( sort.ispc "--target=avx2" )
set( SORT_SOURCES
  ${CMAKE_CURRENT_BINARY_DIR}/ispc/sort.ispc.o
  sort.cpp
  sort_serial.cpp
  sort_openmp.cpp
  tasksys.cpp )
set( SORT_HEADERS
  ${CMAKE_CURRENT_BINARY_DIR}/ispc/sort.ispc.h )

set( SORT_LINK_LIBRARIES
  PUBLIC ${CMAKE_THREAD_LIBS_INIT}
  )

common_application( sort )
